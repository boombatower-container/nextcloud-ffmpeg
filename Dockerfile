FROM nextcloud:production

# Video previews!
RUN apt update && apt install -y ffmpeg libmagickcore-6.q16-6-extra && apt clean
